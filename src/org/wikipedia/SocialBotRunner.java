package org.wikipedia;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import java.util.zip.GZIPInputStream;
import javax.security.auth.login.*;

import org.wikipedia.Wiki.Revision;
import org.wikipedia.Wiki.User;

public class SocialBotRunner {
	
	//ASKS FOR PASSWORD
	
	public static void main(String[] args) throws IOException, CredentialNotFoundException {
		
		System.out.print("Enter login password:");
		char[] password = null;
		
		while (password == null){
				
			try {BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
				 password = input.readLine().toCharArray();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		//CONNECTS TO WIKIPEDIA AND LOGS IN AS SOCIALBOT
					
		Wiki wiki = null;
		
		try
		{
	       wiki = new Wiki("en.wikipedia.org"); // create a new wiki connection to en.wikipedia.org
	    // wiki.setThrottle(1000); 
	       wiki.login("SocialBot", password); // log in as user SocialBot, with the specified password
		}
		catch (FailedLoginException e){
			e.printStackTrace();
			System.out.print("Failed Login!");
		}
		
		catch (UnknownHostException e) { 
			System.out.print("No Internet??");
		}
		
	// TASK 1: creates a new instance of RevisionsList, which is the latest list 
	// of newcomer's revisions to GeneWiki.
		
		RevisionsList list = new RevisionsList(wiki, 24); // in last 24 hours
		HashMap<Revision, ArrayList<User>> links;
		
		for (int i=0 ; i<list.size(); i++){               
			Revision r = list.get(i);
			System.out.println(r);  //  prints out each revision for my sake
			// TODO: get oldUsers for r, and add to hashmap 'links'. 
		}
		
		System.out.println("Total new revisions found: "+list.size());
		// creates an OldHandsList instance for each edit, which finds old users who are likely to be interested in the edit.
		
		
		// TODO implement task 3 - send message to oldhands
		
		
		
		
		
		
		
		
		
		/* TESTING CODE: prints most recent edit made to a page
	    String testPage = "Insulin";
		Revision newEdit = wiki.getTopRevision(testPage);
		String date = wiki.calendarToTimestamp(newEdit.getTimestamp());
		String user = newEdit.getUser();
		
		System.out.println(user +" edited "+testPage+" on "+date+"."); */     
	      
		
	
		
		
		// these exceptions are fatal - we need to abandon the task
				// Exceptions that will probably need catching eventually
				
				
				/*
				
				catch (CredentialNotFoundException e)
				{
					e.printStackTrace();
					System.out.print("Tried to do something that we can't, due to lack of credentials...?");
				}
				
				
				 
				catch (CredentialExpiredException e)
				{
					e.printStackTrace();
					System.out.print("Expiry of Cookies!");
				   // deal with the expiry of the cookies
				}
				catch (AccountLockedException e)
				{
				   // deal with being blocked
					e.printStackTrace();
					System.out.print("Account has been blocked!");
				}
				catch (IOException e)
				{
					e.printStackTrace();
					System.out.print("IO Error!");
				}
				*/
				
	

	
	
	
}
	}


