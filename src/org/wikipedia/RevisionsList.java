package org.wikipedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.security.auth.login.CredentialNotFoundException;

import org.wikipedia.Wiki.Revision;
import org.wikipedia.Wiki.User;

public class RevisionsList extends ArrayList<Revision>{
	private Wiki wiki;
	
	// creates list of all revisions made in the last specified number of hours
	public RevisionsList(Wiki wiki, Integer hours) throws CredentialNotFoundException, IOException{
		this.wiki = wiki;
		
		Calendar rightNow = Calendar.getInstance(), hoursAgo = Calendar.getInstance();
		hoursAgo.add(Calendar.HOUR_OF_DAY, -hours);
		// Get all pages in gene wiki -  // not sure if wiki.whatLinksHere should be used instead
		String[] gwpages = wiki.whatTranscludesHere("Template:GNF Protein box", 0);
		
		for (String page : gwpages) {
			// get most recent edits for every gene wiki page, 
			Revision[] edits = wiki.getPageHistory(page, rightNow, hoursAgo);
			if (edits!=null){
				for (Revision edit : edits) {
				  //if valid, add edit to output list
					if (isValid(edit)) this.add(edit);
				}
			}		
		}	
	}
	
	// tests if an edit is valid i.e. made by new/inactive user, is in mainspace, isn't minor
	private boolean isValid(Revision r) throws IOException{
		String page = r.getPage();
		boolean isInMainspace = wiki.namespace(page)==0;
		boolean isMinor = r.isMinor();
		return (isInMainspace && !isMinor && hasValidEditor(r));
	}
	
	
	// tests if an edit's author is new / inactive, not bot, not blocked & not anon
	private boolean hasValidEditor(Revision r) throws IOException{
	
		if (r.isBot() || !wiki.userExists(r.getUser())) return false;  // invalid if user is a bot, or is anonymous	
		
		User user = wiki.getUser(r.getUser());       // only creates user object if they aren't anon
		try {
			if (user.isBlocked()) return false;     // invalid if user has been blocked 
		} 
		catch (IOException e) {                   // TODO catches an error that occurs when fetching blocklist for some users... needs investigating
			e.printStackTrace();
		}
		
		
	    // create date for 2 weeks ago
		HashMap<String,Object> userinfo = user.getUserInfo();
		Calendar shortTimeAgo = Calendar.getInstance();
		shortTimeAgo.add(Calendar.WEEK_OF_YEAR, -2);    
		
		Calendar dateCreated = (Calendar) userinfo.get("created");
		boolean isNew = dateCreated.after(shortTimeAgo); // user has registered recently
		//(relative to NOW, as opposed to the date of the edit. Assumes we are dealing with very recently made edits (i.e. in last 24 hours)
		
		boolean isInactive = user.countEdits() < 50; // user is inactive if edit count<50
		// (includes posts to talkpages etc, as well as edits to content)

		/*
		 * TODO: consider other ways of identifying users who may need encouragement.
		 * e.g. inactive users could be those who made their last edit a long time ago.
		 * (but then some users could be established wikipedians who are intentionally inactive, 
		 * so they wouldn't need encouragement)  
		 */
		
		return (isNew || isInactive);	// new users may be very active, but are still picked up
		}		
	}
	
	
	/*  OLD CODE THAT USED A WATCHLIST - but took longer because watchlist needed updating regularly, in case of new gw pages. Above code uses a current list of gwpages everytime.
	
	public void updateWatchlist(int amount) throws CredentialNotFoundException, IOException{    //add a specified number of Gene Wiki pages to watchlist
		
		for (int i=0; i<amount; i++){ 
			wiki.watch(gwpages[i]);
		}
		watchlist = wiki.getRawWatchlist();   // updates saved watchlist
	}
	
	public void updateChanges() throws CredentialNotFoundException, IOException{
		this.gwchanges = wiki.watchlist(false, 0);
	}
	*/


