package org.wikipedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;

import org.wikipedia.Wiki.*;
//import org.wikipedia.Wiki.Revision;
//import org.wikipedia.Wiki.User;

public class OldHandsList extends ArrayList<User>{
	
	private Wiki wiki;
	private Revision edit;
	private String page;
	
	public OldHandsList(Wiki wiki, Revision r) {
		this.wiki = wiki;
		this.edit = r;
		this.page = r.getPage();
		findOldHands();
	}// Collection users;
	// Collection users;
	private void findOldHands(){
		// TODO
	}

	// is a valid oldhand if they are active, have been registered for over a year, and aren't blocked.
	private boolean isValid(User user) throws IOException {
		
		if (user.isBlocked() || user.blockLog().length > 0) return false; 
		// or maybe it's ok if they've been blocked once or twice?
		
		Revision[] edits = user.contribs();
		Revision lastEdit = edits[edits.length];
		Calendar dateOfLastEdit = (Calendar) lastEdit.getTimestamp();
		Calendar shortTimeAgo = Calendar.getInstance();
		shortTimeAgo.add(Calendar.WEEK_OF_YEAR, -1);
		boolean isActive = (user.countEdits() > 100 && dateOfLastEdit.after(shortTimeAgo));
		
	    HashMap<String,Object> userinfo = user.getUserInfo();
		Calendar longTimeAgo = Calendar.getInstance();
		longTimeAgo.add(Calendar.WEEK_OF_YEAR, -52);    
		Calendar dateCreated = (Calendar) userinfo.get("created");
		boolean isOld = dateCreated.before(longTimeAgo); // isOld if registered over a year ago
		
		return (isActive && isOld);
		
		// may also make use of (user.isA(group))
		
	}		
	
	
	private User findPageCreator() throws IOException{
		/* returns the user that made the first edit to same page (ie created the page). 
		   TODO: Alternatively could return the first valid olduser to edit the page.
		   Consider ignoring creations that happened a very long time ago.
		   Consider ignoring creation revisions that were very small (eg creation of a stub article).
		*/
		
		Revision firstRev = wiki.getFirstRevision(page);	
	    // firstRevision.isNew() - possible check to make sure 'firstrevision' is indeed the 
		// creation of the page. method is buggy however (see wiki.java doc)
		if (firstRev.isMinor() || firstRev.isBot() || !wiki.userExists(firstRev.getUser())) return null;   // if minor/bot/anon
		User creator = wiki.getUser(firstRev.getUser());
		if (isValid(creator)) return creator;
		else return null;	
	}
	
	
	//returns the last olduser who edited the same page 
	//( OR same section, eg if article is large with many sections)
	private User findLastEditor(){
		
		// TODO
		
	}
	
	// return the olduser that has made the largest number of edits to the same page
	private User findBiggestEditor(){
		
		// TODO
		
	}
	
	private User findTalkpageContributors() throws IOException{
		String talkpage = wiki.getTalkPage(page);
		// TODO
	}
	
	
}


