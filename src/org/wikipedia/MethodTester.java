package org.wikipedia;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import java.util.zip.GZIPInputStream;
import javax.security.auth.login.*;

import org.wikipedia.Wiki.User;
import org.wikipedia.Wiki.*;

public class MethodTester {
	
	public static void main(String[] args) throws IOException {
		
		Wiki test = new Wiki();
		
		// TESTING PAGES
		
		Revision firstR = test.getFirstRevision("Keratin 9");  // is first rev the one that created page??
		// 
		System.out.println("first revision is: "+firstR);    // should be  01:56, 21 August 2004‎ Fuelbottle (talk | contribs)‎ . . (280 bytes) (+280)‎
		System.out.println("Result of isNew: "+firstR.isNew());    // isNew always returns false for revisions before 2007
			
			
		
		// TESTING USERS
		
		String username = "‎12.230.39.130";	 // name of the user to be tested    eg "KimRT"  or "‎12.230.39.130"
				
		System.out.println("User exists? "+test.userExists(username));     // RESULT OF USEREXISTS METHOD
		String username2 = URLEncoder.encode(username, "UTF-8");
		String text = test.fetch(test.query + "list=users&ususers=" + username2, "userExists");
        // if contains("missing=\"\"");  it is supposedly non-existent and userExists should return false..#
		//UPDATE: the fix for this bug is to replace missing with invalid. (r86 of wiki.java)
		System.out.println("text is: "+text);
		
		User user = test.getUser(username); // getUser shouldn't be called on anon users (ie IP addresses)
		HashMap<String, Object> info = user.getUserInfo();	// because then this fails (can't get edit count for anon users, it seems)
		
		
		
		
		// test to see if user is blocked   - seems to fail for these users: TODO add users as they appear
		
		try {
			System.out.println("User blocked? "+user.isBlocked());
		} catch (IOException e) {
			System.out.println(user+" throws block error");
			e.printStackTrace();
		}
		
		System.out.println("caught exception and continuing to execute");
		
		
		
		
		
		/*String[] categories = {""};
		try {
			categories = test.getCategories("Insulin");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		
		 for (String category : categories){
			System.out.println(category+", ");
		}
		*/
	
	}
	
	
	
	

}

